package Navegador;

import java.util.ArrayDeque;
import java.util.Iterator;


/**
 * La clase Pila con sus metodos se encarga de dar funcionalidad a la pila (stack).
 * Se compone de un ArrayDeque<E> que hara de pila y se encargara de almacenar los datos recibidos, asi como
 * eliminarlos o manipularlos.
 * @author Daniel Acosta Romero
 * @version 06/12/2020
 *
 * @param <E> Elemento.
 */
public class Pila<E> implements Iterable<E>{
    private ArrayDeque<E> pila = new ArrayDeque<>();

    /**
     * Metodo para comprobar si la pila esta vacia.
     *
     * @return Retorna true si esta vacia, false si por lo contrario contiene elementos.
     */
    public boolean empty(){
        return pila.isEmpty();
    }

    /**
     * Metodo encargado de devolver el ultimo elemento introducido en la pila.
     *
     *
     * @return Retorna el ultimo elemento de la pila.
     * @throws PilaBuidaException Excepcion lanzada si la pila se ecuentra vacia.
     */
    public E peek() throws  PilaBuidaException{
        if (empty()){
            throw new PilaBuidaException();
        }
        return pila.peek();
    }


    /**
     * Metodo encargado de eliminar el ultimo elemento de la pila.
     *
     * @return Retorna el ultimo elemento de la pila que tambien ha sido borrado.
     * @throws PilaBuidaException Excepcion lanzada si la pila se ecuentra vacia.
     */
    public E pop() throws PilaBuidaException{
        if (empty()){
            throw new PilaBuidaException();
        }
        return pila.pop();
    }

    /**
     * Metodo encargado de agregar un nuevo elemento e la pila.
     *
     * @param e Elemento recibido como valor desde la llamada al metodo.
     */
    public void push(E e){
        pila.push(e);
    }


    /**
     * Metodo encargado de vaciar la pila
     *
     */
    public void removeAllElements(){
        pila.clear();
    }



    /**
     * Iterador que permitira iterar sobre la pila.
     *
     * @return Retorna todos los elementos de la pila.
     */
    @Override
    public Iterator<E> iterator() {
        return pila.iterator();
    }
}
