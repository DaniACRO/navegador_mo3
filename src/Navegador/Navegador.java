package Navegador;

import java.util.*;


/**
 * La clase Navegador se encarga de trabajar internamente con una pila para ir atras, otra para ir adelante,
 * una lista para para el historial y un mapa para almacenar la cantidad de urls visitadas.
 * Esta compuesta por dos objetos de tipo Pila (uno principal y otro auxiliar), un ArrayList y un Treemap. Tambien tiene metodos especificos.
 * Tiene un atributo tipo String llamado url que se encarga de almacenar momentaneamente las url, valga la redundancia, para manipularlas
 * en los diferentes metodos.
 * Tambien dispone de un constructor propio, getters y setters.
 * @author Daniel Acosta Romero
 * @version 06/12/2020
 */
public class Navegador {
    private String url;

    public Navegador(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    ArrayList<String> historial = new ArrayList<>();
    TreeMap<String, Integer> visitades = new TreeMap<>();

    Pila pilaPrincipal = new Pila(); //Enrere
    Pila pilaAuxiliar= new Pila();//Endavan


    /**
     * Metodo para itroducir una nueva URL en la pila principal. Manda una URL con la pila principal al metodo push() de la clase Pila para agregar el nuevo elemento.
     * Llama al metodo removeAllElements() de la clase Pila con la pila auxiliar para eliminar su contenido. Esto es para no poder ir adelante cuando se introduce una nueva URL.
     * agrega la nueva URL visitada al historial.
     * Da al atributo url el valor de novURL.
     * Manda al metodo comprobarKeyTreeMapRepeat() la nueva URL para comprobar si es la primera vez que se visita.
     *
     * @param novaURL URL que se mandara al metodo push() de la clase Pila. Tambien se manipulara en esta misma clase Navegador.
     */
    public void anarA(String novaURL) {
        pilaPrincipal.push(novaURL);
        pilaAuxiliar.removeAllElements();
        historial.add(novaURL);
        setUrl(novaURL);

        comprobarKeyTreeMapRepeat(novaURL);
    }


    /**
     * Metodo para acceder a la URL visitada anteriormente. Alacena en la pila auxiliar le URL eliminada de la pila principal.
     * Manda al metodo comprobarKeyTreeMapRepeat() la URL para aumentar su numero de visitas.
     * Da al atributo url el valor de pilaPrincipal.peek() (una url).
     * Muestra la URL.
     * Si la pila esta vacia, salta una excepcion que lo que hace es llamar al metodo paginaInici()
     *
     */
    public void enrere() {
        try {
            pilaAuxiliar.push(pilaPrincipal.pop());
            setUrl((String) pilaPrincipal.peek());

            comprobarKeyTreeMapRepeat((String)pilaPrincipal.peek());

            System.out.println(pilaPrincipal.peek());
        } catch (PilaBuidaException e) {
            paginaInici();
        }

    }

    /**
     * Metodo encargado de mostrar la pagina de inicio cuando la pila principal esta vacia.
     * Da a el atributo utl el valor de "http://www.itb.cat".
     * Muestra la url.
     * Manda al metodo comprobarKeyTreeMapRepeat() la URL para aumentar su numero de visitas.
     */
    public void paginaInici(){
        setUrl("http://www.itb.cat");
        System.out.println(url);
        comprobarKeyTreeMapRepeat(url);
    }


    /**
     * Metodo para ir adelante y acceder a la URL en la cual nos encontrabamos antes de llamar al metodo enrere(). Alamacena en la pila principal
     * la URL eliminada de la pila auxiliar.
     * Muestra la ultima URL de la pila principal.
     * Llama al metodo comprobarKeyTreeMapRepeat() mandandoles la ultima URL de la pila principal para aumentar su número de visitas.
     *
     */
    public void endavant() {
        try {
            pilaPrincipal.push(pilaAuxiliar.pop());
            System.out.println(pilaPrincipal.peek());
            setUrl((String) pilaPrincipal.peek());

            comprobarKeyTreeMapRepeat((String)pilaPrincipal.peek());

        } catch (PilaBuidaException e) {
            e.getMessage();
        }
    }


    /**
     * Metodo que itera sobre el ArrayList historial y muestra todas las URL visitadas.
     *
     */
    public void veureHistorial(){
        historial.forEach(System.out::println);
    }


    /**
     * Metodo que itera sobre el TreeMap visitades que muestra el numero de visitas que tiene cada URL accedida.
     * Contiene un objeto Comparator que llama a la clase ordenarPerValue, un TreeMap donde se cargaran los datos ordenados
     * y un bucle for each para mostrar la lista de visitas.
     *
     */
    public void veureVisitades(){
        Comparator<String> comparador = new ordenarPerValue(visitades);
        TreeMap<String, Integer> visitadesSortByValue = new TreeMap<>(comparador);
        visitadesSortByValue.putAll(visitades);


        Set<Map.Entry<String, Integer>> mostrarVisitadas = visitadesSortByValue.entrySet();
        for (Map.Entry<String, Integer> x : mostrarVisitadas) {
            System.out.println(x.getKey() + " ---> " + x.getValue() + " visites");
        }
    }


    /**
     * Metodo que recibe una URL i comprueba si estaba agregada ateriormente en el TreeMap visitades.
     * Si el mapa ya contenia la URL recibida, suma 1 a su contador particular de visitas (TreeMap<String, Integer> visitades = new TreeMap<>()).
     * Si por lo contrario el mapa no contenia dicha URL, se agrega como key la URL y se inicializa su contador (value) en 1
     *
     * @param novaURL URL en froma de String para conocer si ya ha sido visitada o no.
     */
    public void comprobarKeyTreeMapRepeat(String novaURL){
        if (visitades.containsKey(novaURL)){
            int contador = visitades.get(novaURL);
            visitades.replace(novaURL, contador, contador+1);
        }else{
            visitades.put(novaURL, 1);
        }
    }

}

