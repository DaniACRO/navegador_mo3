package Navegador;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Clase para probar el correcto funcionamiento del proyecto Navegador Web.
 * Contiene un objeto Navegador para ir llamando a los metodos de la clase con el mismo nombre.
 * Contiene el main para poder ejecutar el programa.
 * @author Daniel Acosta Romero
 * @version 06/12/2020
 */
public class ProvaNavegador {
    Navegador morcillaFirefox = new Navegador("http://www.itb.cat");


    public static  void main(String[] args) {
        ProvaNavegador programa = new ProvaNavegador();
        programa.inici();
    }

    /**
     * Metodo que llama al metodo menu().
     *
     */
    public void inici(){
        menu();
    }

    /**
     * Metodo que se compone de un menu para que el usuario interactue con el programa.
     * El menu se compone de 6 posibles opciones, las 5 primeras llaman cada una a un metodo especifico de la clase Navegador. La ultima opcion finaliza el programa.
     *
     */
    public void menu(){
        Scanner lector = new Scanner(System.in);
        boolean exit = false;
        int choice = 0;

        while (!exit){
            System.out.println("[1] Go to" +
                    "\n[2] Go back" +
                    "\n[3] Go forward" +
                    "\n[4] View history" +
                    "\n[5] View visits" +
                    "\n[6] Finish");

            while (!exit){
                try {
                    choice = lector.nextInt();
                    exit = true;
                }catch (InputMismatchException i){
                    System.out.println("REMEMBER: Have to be a number");
                    lector.nextLine();
                }
            }
            exit = false;

            switch (choice){
                case 1:
                    boolean pase = false;
                    String regex = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
                    while (!pase){
                        System.out.print("Enter the URL: ");
                        String url = lector.next();
                        if (url.matches(regex)){
                            morcillaFirefox.anarA(url);
                            pase = true;
                        }
                    }
                    break;
                case 2:
                    morcillaFirefox.enrere();
                    break;
                case 3:
                    morcillaFirefox.endavant();
                    break;
                case 4:
                    morcillaFirefox.veureHistorial();
                    break;
                case 5:
                    morcillaFirefox.veureVisitades();
                    break;
                case 6:
                    System.out.println("See you soon!");
                    exit = true;
                    break;
                default:
                    System.out.println("REMEMBER: Only numbers between 1 and 6");
                    break;

            }
        }
    }
}
