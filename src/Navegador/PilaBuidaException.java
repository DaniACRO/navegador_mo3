package Navegador;


/**
 * Excepcion presonalizada que comprueba si la pila esta vacia.
 * @author Daniel Acosta Romero
 * @version 06/12/2020
 */
public class PilaBuidaException extends Exception {
    /**
     * Metodo que no hace nada, vacío.
     *
     */
    public PilaBuidaException() {
    }

}
