package Navegador;

import java.util.Comparator;
import java.util.TreeMap;

/**
 * Clase encargada unica y exclusivaemente de comparar y ordenar los valores de un mapa
 * Se compone de un mapa auxiliar en el que se introduciran los datos del mapa a ordenar y un metodo que compara sus valores y los ordena
 * de mayor a menor. Retorna los valores ordenados.
 */
public class ordenarPerValue implements Comparator<String> {
    TreeMap<String, Integer> mapAux = new TreeMap<>();

    /**
     * Recibe los datos de un map y los introduce en un mapa auxiliar.
     * @param map Mapa auxiliar para manipular datos sin tocar el original.
     */
    public ordenarPerValue(TreeMap<String, Integer> map) {
        mapAux.putAll(map);
    }

    /**
     * Compara los valores del mapa auxiliar devolviendo un -1 si el valor comparado es mayor que el siguiente o un 1 si es menor.
     * @param s1 Primera clave a comparar del mapa
     * @param s2 Segunda clave a comparar del mapa
     * @return Retorna un -1 si s1 es mayor o = que s2 o un -1 si es menor. Asíordena por valores
     */
    /*Ordena los valores de mayor a menor*/
    @Override
    public int compare(String s1, String s2) {
        int aux;
        if (mapAux.get(s1) >= mapAux.get(s2)) {
            aux = -1;
        } else {
            aux = 1;
        }
        return aux;
    }


}
