package Navegador;
/**
 * Clase main para ejecutar el JFrame de la clase Mozilla
 * @author Daniel Acosta Romero
 * @version 06/12/2020
 */
public class MainMozilla {
    public static void main(String[] args) {
        Mozilla m = new Mozilla();
        m.setVisible(true);
    }
}
